const { app, BrowserWindow } = require('electron')
const path = require('path')

//TODO should run ./driver.js

let slideValue = 50

const createWindow = () => {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
        nodeIntegration: true, //security flaw only if execute remote module, permit ipc
        contextIsolation : false, //same
      preload: path.join(__dirname, 'preload.js')
    },
    autoHideMenuBar: true //TODO should reformat the menubar for ours system
  })

  win.loadFile('index.html')

    win.openDevTools()

    win.webContents.on('did-finish-load', () => {
        win.webContents.send('ping', 'whoooooooh!')
    })
}

app.whenReady().then(() => {
  createWindow()

  launchLogic()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})

function launchLogic() {
    setTimeout(function () {
            slideValue += 1
            if (slideValue > 100) {
                slideValue -= 100
            }
            win.webContents.send('ping', slideValue)
            //win.webContents.executeJavaScript("alert('Hello There!');");
            launchLogic()
        }
        , 50);
}
