# POC GUI

Test for the gui with electron.js

The main.js and the front are two separate threads, so an animation controlled from the backend have been coded to demonstrate the possibility to make com' between front and back.

Electron.js seems to be very appropriate to handle GUI. But the main question is how to handle a communication with the driver (C++ software).

So this project would mainly shows how to communicate with the driver.

## Getting-started (recreate this project)

You can use this tutorial : https://www.electronjs.org/docs/latest/tutorial/quick-start .

## Possibilities for IPC (Inter-process communication)

On Windows there are several [ways](https://docs.microsoft.com/en-us/windows/win32/ipc/interprocess-communications) to achieve it.

* Clipboard : need the user interaction
* COM : as used for serial port
* Data Copy : pointers on shared memory
* DDE : not really efficient
* File mapping : synchronization needed
* Mailslot : like mails
* Pipes (anonymous and named) : pretty old and tty-like
* RPC : call functions
* AF_unix Windows sockets : unix-like sockets, unsupported by nodejs
* TCP/IP sockets : socket but network oriented

There is no only one solution. We would have liked to use AF_UNIX sockets but nodejs (precisely its dependency libuv) does not support it yet. But TCP/IP socket is appropriate, fast (localhost optimization) and simple. Only the port semantic can be seen as a flaw, where a socket name (or path) would be better.

A snippet in driver.js has been written for IPC using TCP/IP.

For now, there will be no handle in case the port is already taken and we will take the port **48651** as it [seems there is no regular conflict](https://www.speedguide.net/port.php?port=48651).

## Build and run

You need to install the dependencies with `npm install` .

Then you can run the app with `npm start` .

Or build it with `npm run make` so you can find the executable under the out/ folder.

## MISC

When closing the app, there is an error dialog box because of the timeout function.

The devtoolbox is activated by default.