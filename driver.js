const { IPCModule } = require('node-ipc')

ipc = new IPCModule

ipc.config.id = "hello"
ipc.config.socketRoot= "C:\\Users\\Axel\\AppData\\Local\\Temp\\worlddeuxx"
ipc.config.appspace = "C:\\Users\\Axel\\AppData\\Local\\Temp\\worlddeuxx"
ipc.config.path = "C:\\Users\\Axel\\AppData\\Local\\Temp\\worlddeuxx"
ipc.config.retry= 1500;

ipc.connectTo(
    'world',
    "C:\\Users\\Axel\\AppData\\Local\\Temp\\worlddeuxx",
    function(){
        ipc.of.world.on(
            'connect',
            function(){
                ipc.log('## connected to world ##', ipc.config.delay);
                ipc.of.world.emit(
                    'app.message',
                    {
                        id      : ipc.config.id,
                        message : 'hello'*255
                    }
                );
            }
        );
        ipc.of.world.on(
            'disconnect',
            function(){
                ipc.log('disconnected from world');
            }
        );
        ipc.of.world.on(
            'app.message',
            function(data){
                ipc.log('got a message from world : ', data);
            }
        );
        ipc.of.world.on(
            'kill.connection',
            function(data){
                ipc.log('world requested kill.connection');
                ipc.disconnect('world');
            }
        );
    }
);
//TODO move ipc part